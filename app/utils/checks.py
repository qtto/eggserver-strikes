from flask import request
from flask_login import current_user, login_fresh, confirm_login
from werkzeug.exceptions import Unauthorized
from werkzeug.http import parse_authorization_header
from functools import wraps
from app.utils.func import get_mod
from app import flask_app, db



def is_bot(request):
    return request.headers.get('authorization') == flask_app.config["API_SECRET_KEY"]


def mod_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if is_bot(request):
            return f(*args, **kwargs)

        if current_user.is_authenticated:
            if current_user.get_is_mod():
                if not login_fresh():
                    if not get_mod(current_user.get_id()):
                        raise Unauthorized()
                    confirm_login()
                return f(*args, **kwargs)

        raise Unauthorized()
    return decorated_function


def login_required(f):
    @wraps(f)
    def decorated_function(*args, **kwargs):
        if is_bot(request) or current_user.is_authenticated:
            return f(*args, **kwargs)
        raise Unauthorized()
    return decorated_function