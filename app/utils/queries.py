from app import flask_app, db
from app.models import Strike, User, Ban, OAuth, Mute
from flask_login import current_user
from functools import reduce
from sqlalchemy import func
from datetime import timedelta

def count_total_strike():
    return Strike.query.count()


def count_active_strike():
    return Strike.query.filter_by(expired=False).count()


def count_site_user():
    return OAuth.query.count()


def count_ban():
    return Ban.query.count()


def count_personal_strike_given():
    return Strike.query.filter_by(created_by=current_user.get_id()).count()


def count_total_strike_points():
    strikes = Strike.query.filter_by(expired=False)\
                          .filter_by(removed=False)
    
    if strikes.count() == 0:
        return 0
    
    else:
        return reduce(lambda x, y: x + y.tier.points, strikes, 0)


def get_total_strike():
    return Strike.query.all()


def get_total_user():
    return User.query.all()


def get_total_users_by_strike():
    return Strike.query.group_by(Strike.user_id).all()


def get_active_strike(can_expire=False):
    # can_expire: only get strikes that can expire
    if can_expire:
        return Strike.query\
                    .filter_by(expired=False)\
                    .filter_by(removed=False)\
                    .filter(Strike.tier.has(can_expire=can_expire))\
                    .order_by(Strike.created_on)\
                    .all()

    return Strike.query\
            .filter_by(expired=False)\
            .filter_by(removed=False)\
            .order_by(Strike.created_on)\
            .all()


def get_personal_strike(id, no_removed=False):
    if no_removed:
        return Strike.query\
                .filter_by(user_id=id)\
                .filter_by(removed=False)\
                .order_by(Strike.created_on).all()

    return Strike.query.filter_by(user_id=id)\
            .order_by(Strike.created_on).all()


def get_single_strike(id):
    return Strike.query.filter_by(id=id).first()


def get_mute_duration(id):
    user = User.query.filter_by(user_id=id).first()
    points = 2
    if user:
        points = user.points
    
    if points == 0 or points == 1:
        return timedelta(0)
    
    if points > 2:
        max_points = db.session.query(func.max(Mute.points)).one()[0]
        if points > max_points:
            points = max_points

    return Mute.query.filter_by(points=points).first().duration


def get_user_id_from_name(name):
    q = User.query.with_entities(User.user_id).filter(User.nick.ilike("%{}%".format(name)))
    if q.count() < 1:
        return False
    return q.first().user_id


def get_user_name_from_id(user_id):
    q = User.query.filter_by(user_id=user_id)
    if q.count() < 1:
        return False

    return q.first().nick


def get_user_from_id(id):
    q = User.query.filter_by(user_id=id)
    if q.count() < 1:
        return False
    return q.first()