import requests
import datetime
from flask import jsonify, url_for
from app import flask_app, db
from app.models import Strike, User, Ban, Tier
import app.utils.queries as queries

BOT_KEY = flask_app.config["BOT_KEY"]
GUILD_ID = flask_app.config["GUILD_ID"]
STRIKE_EXPIRATION = flask_app.config["STRIKE_EXPIRATION"]

def get_endpoint(endpoint, params=None):
    headers = {"Authorization": "Bot {}".format(BOT_KEY)}
    url = "https://discordapp.com/api/guilds/{}".format(GUILD_ID)
    url += "/{}".format(endpoint)

    return requests.get(url, headers=headers, params=params)


def make_avatar(user):
    return "//cdn.discordapp.com/avatars/{}/{}".format(user.user_id, user.avatar) \
                if user.avatar else url_for('static', filename='img/default-avatar.png')


def update_user(user):
    if type(user) == str:
        user = queries.get_user_from_id(user)
        if not user:
            return False
    
    user_req = get_endpoint("members/{}".format(user.user_id))
    if not user_req.ok:
        return False

    user_info = user_req.json()

    if not user_info:
        return False
    
    try:
        user.user_id = user_info["user"]["id"]
        user.nick = user_info["user"]["username"]
        user.avatar = user_info["user"]["avatar"]

        db.session.commit()
    except:
        return False

    return user
    

def get_mod(id):
    '''Checks if user is a mod'''
    # get info on current user
    user_req = get_endpoint("members/{}".format(id))
    if not user_req.ok:
        return False
    
    user = user_req.json()
    if not user["roles"]:
        return False

    # get id of mod role
    role_req = get_endpoint("roles")
    if not role_req.ok:
        return False

    roles = role_req.json()
    
    mod_role = next(filter(lambda x: x['name'] == 'Mod', roles))

    # one last check
    if not mod_role["mentionable"] and not mod_role["hoist"]:
        return False

    # user is mod
    return mod_role["id"] in user["roles"]


def get_bans():
    '''Updates the banlist'''
    r = get_endpoint("bans")
    if not r.ok:
        return False

    bans = r.json()
    ids = []

    # go over all bans discord returned
    for item in bans:
        user_id = item["user"]["id"]
        ids.append(item["user"]["id"])

        # Check if ban is already in db
        if db.session.query(Ban.user_id).filter_by(user_id=user_id).scalar() is not None:
            continue

        # Check if the banned person is a site user
        if db.session.query(User.user_id).filter_by(user_id=user_id).scalar() is not None:
            ban = Ban (
                user_id=item["user"]["id"],
                reason=item["reason"]
            )

        # Else add them to the db
        else:
            ban = Ban (
                user_id=item["user"]["id"],
                reason=item["reason"],
                user=User(
                    user_id=item["user"]["id"],
                    nick=item["user"]["username"],
                    avatar=item["user"]["avatar"]
                )
            )

        db.session.add(ban)

    for user_id in db.session.query(Ban.user_id):
        if user_id[0] not in ids:
            db.session.query(User.user_id).filter_by(user_id=user_id[0]).delete()

    db.session.commit()
        
    return True


def sanity_check():
    '''Fixes decay counts'''
    strikes = queries.get_total_strike()

    for strike in strikes:
        if strike.times_expired > strike.tier.points:
            strike.times_expired = strike.tier.points
            db.session.add(strike)
    
    db.session.commit()
    

def id_to_user(id):
    '''tries to convert an id to a user'''
    user = queries.get_user_from_id(id)
    if user:
        update_user(user)
        return user
    
    user_req = get_endpoint("members/{}".format(id))
    if not user_req.ok:
        return False

    user_info = user_req.json()

    if not user_info:
        return False
    
    user = User(
            user_id=user_info["user"]["id"],
            nick=user_info["user"]["username"],
            avatar=user_info["user"]["avatar"]
        )

    db.session.add(user)
    db.session.commit()

    return user


def get_expired():
    '''Expires active strikes that have expired'''
    amount_expired = 0

    def expire(strike):
        '''Expires the given strike'''
        if not strike.next_expire:
            #if the strike doesnt expire at all don't do it, obviously
            return

        if strike.next_expire > datetime.datetime.now():
            #skip the strike if it shouldn't expire
            return

        if strike.times_expired < strike.tier.points:
            #only expire if it still can
            strike.times_expired += 1
            strike.last_expired = strike.next_expire
        
        if strike.times_expired >= strike.tier.points:
            strike.expired = True
        
        else:
            strike.next_expire = strike.next_expire + STRIKE_EXPIRATION

        return True

    # All current active strikes
    active = queries.get_active_strike(can_expire=True)

    if len(active) == 0:
        return 0

    for strike in active:
        if expire(strike):
            amount_expired += 1
        
    db.session.commit()

    return amount_expired


def get_next_expire(strike):
    '''Returns when the strike next expires'''
    return strike.next_expire


def format_single_strike(strike, for_mod=False):
    '''formats a single strike into a dict'''
    strike_info = {
        "id": strike.id,
        "reason": strike.reason,
        "attachment": strike.attachment,
        "created_on": strike.created_on,
        "times_expired": strike.times_expired,
        "tier": strike.tier.name,
        "points": strike.tier.points,
        "removed": strike.removed,
        "expired": strike.expired,
    }

    if not strike.expired:
        strike_info["next_expire"] = strike.next_expire

    if for_mod:
        strike_info["created_by"] = strike.created_by
        
        if strike.removed:
            strike_info["removed_by"] = strike.removed_by
    
    return strike_info


def format_user_info(user=None, strike=None):
    '''formats user info into json. if a strike is passed no user is assumed'''
    if not user:
        user = id_to_user(strike.user_id)

    if user:
        return {
            "name": user.nick,
            "strike_points": user.points,
            "avatar": make_avatar(user),
            "id": user.user_id
        }

    return {
        "name": strike.user_id,
        "strike_points": strike.tier.points,
        "avatar": url_for('static', filename='img/default-avatar.png'),
        "id": strike.user_id
    }


def format_strikes(strikes, for_mod=False, single_user=False):
    '''Formats strikes in json format for the api'''
    if single_user:
        if strikes[0].user:
            user_json = format_user_info(user=strikes[0].user)
        else:
            user_json = format_user_info(strike=strikes[0])
        
        strike_json = []
        for strike in strikes:
            strike_json.append(format_single_strike(strike, for_mod))
        
        return {"user": user_json, "strikes": strike_json}


    # When it's not for a single user
    json = []
    for strike in strikes:
        json.append({
            "user": format_user_info(user=strike.user) if strike.user else format_user_info(strike=strike),
            "strike": format_single_strike(strike, for_mod)
        })

    return json


def add_strike(strike):
    '''Adds strike to db'''
    # create user if it doesnt exist
    if not id_to_user(strike.user_id):
        user = User(
            user_id=strike.user_id,
            nick=strike.user_id,
            avatar=None
        )

        db.session.add(user)

    if Tier.query.filter_by(tier_id=strike.tier_id).first().can_expire:
        user_strikes = queries.get_personal_strike(strike.user_id)

        if user_strikes:
            #if there's earlier strikes then we'll have to calculate when this one expires
            for last_strike in reversed(user_strikes):
                if last_strike.expired:
                    break
                
                if not last_strike.tier.can_expire and not last_strike.removed:
                    #be fair, new strikes can expire even if the last one cannot
                    if last_strike.created_on + last_strike.tier.points*STRIKE_EXPIRATION > datetime.datetime.now():
                        strike.next_expire = last_strike.created_on + last_strike.tier.points*STRIKE_EXPIRATION + STRIKE_EXPIRATION
                    break

                if not last_strike.removed:
                    #if the last strike can expire then calculate when the new one gets its turn
                    strike.next_expire = last_strike.next_expire + (last_strike.tier.points-last_strike.times_expired-1)*STRIKE_EXPIRATION\
                        + STRIKE_EXPIRATION
                    break
    
        if not strike.next_expire:
            strike.next_expire = datetime.datetime.now() + STRIKE_EXPIRATION

        
    db.session.add(strike)
    try:
        db.session.commit()
    except:
        return False
    
    return True


def migrate_db():
    '''migrates the db to a version with better expiry code'''
    db.create_all()
    strikes = queries.get_total_strike()

    for strike in strikes:
        strike.expired = False
        strike.times_expired = 0
        strike.last_expired = None
        strike.next_expire = None

    users = []

    for strike in strikes:
        user = strike.user_id
        if user in users:
            continue
        
        users.append(user)
        user_strikes = queries.get_personal_strike(user)

        for user_strike in user_strikes:
            if not user_strike.tier.can_expire:
                #dont set date on strikes that can't expire
                continue
            
            if user_strike.removed:
                continue
                
            if user_strikes[0] == user_strike:
                #if it's the first strike set the date
                user_strike.next_expire = user_strike.created_on + STRIKE_EXPIRATION
            
            else:
                i = user_strikes.index(user_strike)
                last_strike = False

                while not last_strike:
                    #make sure to grab the last strike that has a date set
                    if i == 0:
                        user_strike.next_expire = user_strike.created_on + STRIKE_EXPIRATION
                        break
                        
                    try:
                        last_strike = user_strikes[i-1]

                        if last_strike.removed:
                            raise ValueError

                        if not last_strike.next_expire:
                            calc_expire = last_strike.created_on + last_strike.tier.points*STRIKE_EXPIRATION
                            if calc_expire > user_strike.created_on:
                                user_strike.next_expire = calc_expire + STRIKE_EXPIRATION
                            else:
                                user_strike.next_expire = user_strike.created_on + STRIKE_EXPIRATION
                        
                        elif last_strike.next_expire:
                            calc_expire = last_strike.next_expire + (last_strike.tier.points-1)*STRIKE_EXPIRATION
                            if calc_expire > user_strike.created_on:
                                user_strike.next_expire = calc_expire + STRIKE_EXPIRATION
                            else:
                                user_strike.next_expire = user_strike.created_on + STRIKE_EXPIRATION

                    except:
                        last_strike = False
                        i -= 1

            db.session.commit()
    
    amount = 1
    while amount > 0:
        amount = get_expired()

    return amount   
