var m = require("mithril")

var Search = require("./objects/Search")

var Nav = {
    view: function() {
        return m("nav", isMod ?  
        [
            m(Search),
            m("a.nav-link", {href: '/active', oncreate: m.route.link}, "active strikes"),
            m("a.nav-link", {href: '/users', oncreate: m.route.link}, "active users"),
            m("a.nav-link", {href: '/all', oncreate: m.route.link}, "all strikes"),
            m("a.nav-link", {href: '/bans', oncreate: m.route.link}, "bans"),
        ] : null)
    }
}
module.exports = Nav