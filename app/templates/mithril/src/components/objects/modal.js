var m = require("mithril")

var Clock = require("./Clock")

var Loader = {
    loading: m(".loading", "Loading..."),
    
    loadStrike: function(endpoint) {
        return m.request({
            method: "GET",
            url: endpoint,
            withCredentials: true,
        })
        .then(Loader.loadedStrike)
    },
    loadedStrike: function(result){
        Loader.loading = null
        console.log(result)
        modal = document.getElementById('modal')
        m.render(modal, [
            m(".modal-header", "Strike information"),
            m("section.fullwidth.points", result.points-result.times_expired + "/" + result.points + " points left"),
            m("section", [
                m(".section-header", "Next expire"),
                m(Clock, {date: result.next_expire})
            ]),
            m("section", [
                m(".section-header", "Created on"),
                m(".section-value", result.created_on)
            ]),
            m("section.fullwidth", [
                m(".section-header", "Reason"),
                m(".section-value", result.reason),
            ]),
            m("section.fullwidth", [
                m(".section-header", "Attachment"),
                m(".section-value", result.attachment),
            ]),
            m("section", [
                m(".section-header", "Created by"),
                m(".section-value", result.created_by),
            ]),
            result.removed_by ? m("section", [
                m(".section-header", "Removed by"),
                m(".section-value", result.removed_by),
            ]) : null
        ])
    },

    loadUser: function(endpoint) {
        return m.request({
            method: "GET",
            url: endpoint,
            withCredentials: true,
        })
        .then(Loader.loadedUser)
    },
    loadedUser: function(result){
        Loader.loading = null
        console.log(result)
        modal = document.getElementById('modal')
        m.render(modal, [
            m(".modal-header", "Strike information"),
            m("section.fullwidth.points", result.points-result.times_expired + "/" + result.points + " points left"),
            m("section", [
                m(".section-header", "Next expire"),
                m(Clock, {date: result.next_expire})
            ]),
            m("section", [
                m(".section-header", "Created on"),
                m(".section-value", result.created_on)
            ]),
            m("section.fullwidth", [
                m(".section-header", "Reason"),
                m(".section-value", result.reason),
            ]),
            m("section.fullwidth", [
                m(".section-header", "Attachment"),
                m(".section-value", result.attachment),
            ]),
            m("section", [
                m(".section-header", "Created by"),
                m(".section-value", result.created_by),
            ]),
            result.removed_by ? m("section", [
                m(".section-header", "Removed by"),
                m(".section-value", result.removed_by),
            ]) : null
        ])
    }
}

var Modal = {
    populate: function(endpoint, type) {
        type === "strike" ? Loader.loadStrike(endpoint) : Loader.loadUser(endpoint)
        modalContainer = document.getElementById('modal-container');
        modal = document.getElementById('modal');
        modalContainer.style.display="flex"
        m.render(modal, Loader.loading)
    }
}

module.exports = Modal