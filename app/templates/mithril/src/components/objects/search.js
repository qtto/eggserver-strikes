var m = require("mithril")

module.exports = {
    handlePress: function(e){
        if(e.keyCode != 13) return;
        if(!e.target.value) return;
        m.route.set("/user/" + e.target.value)
    },
    view: function() {
        return m("input.nav-search", {placeholder: "🕵️ go to user", onkeyup: this.handlePress})
    }
}