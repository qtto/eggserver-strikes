var m = require("mithril")

var Clock = {
    getTimeUntil: function() {
        if (this.timeUntil === null) return null

        return Math.floor(this.timeUntil/(1000*60*60*24)) > 0 ? Math.floor(this.timeUntil/(1000*60*60*24)).toString() + " days, " +
        Math.floor((this.timeUntil/(1000*60*60))%24).toString() + " hours" :

        Math.floor((this.timeUntil/(1000*60*60))%24) > 0 ? Math.floor((this.timeUntil/(1000*60*60))%24).toString() + " hours, " +
        Math.floor((this.timeUntil/1000/60)%60).toString() + "minutes" :
        
        Math.floor((this.timeUntil/1000/60)%60) > 1 ? Math.floor((this.timeUntil/1000/60)%60).toString() + "minutes" :
        "< 1m"
    },

    oninit: function(vnode) { 
        this.timeUntil = vnode.attrs.date ? new Date(vnode.attrs.date) - new Date() : null
        this.start = function(){
            setInterval(function() {
                m.redraw()
            }, 1000)
        }
        this.start()
    },
    
    view: function() {
        return m(".clock", this.getTimeUntil())
    }
}

module.exports = Clock