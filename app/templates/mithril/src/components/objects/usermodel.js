var m = require("mithril")

var Progressbar = require("./Progressbar")

var UserModel = {
    oninit: function(vnode) {
        this.user = vnode.attrs.user
        this.subunit = true ? vnode.attrs.subunit : false
    },
    view: function() {
        return m("a", {href: "/user/" + this.user.id, oncreate: m.route.link}, [
            m("div.user-wrap", [
                m("div.user", [
                    m(".avatar", {style: {background: 'url('+this.user.avatar+') #fff', backgroundSize: '1.3rem 1.3rem'}}),
                    m(".text-wrap", [
                        m(".username", this.user.name),
                        this.subunit ? null : m(".points", this.user.points + "/12"),
                    ]),
                    this.subunit ? null : m(Progressbar, {val:this.user.points, max:12}),
                ]),
            ])
        ])
    }
}

module.exports = UserModel