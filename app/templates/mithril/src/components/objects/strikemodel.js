var m = require("mithril")

var Clock = require("./Clock")
var Progressbar = require("./Progressbar")
var UserModel = require("./UserModel")

var StrikeModel = {
    classList: function(strike){
        var list = ""
        if(!strike.next_expire) list += ".no-expire"
        if(strike.expired) list += ".expired"
        return list
    },
    oninit: function(vnode) {
        this.strike = vnode.attrs.strike
        this.user = vnode.attrs.user ? vnode.attrs.user : null
        this.points = this.strike.points - this.strike.times_expired
        this.expires = this.strike.next_expire ? this.strike.next_expire : null
    },
    view: function() {
        return m("div.strike-wrap"+this.classList(this.strike), [
                this.user ? m(UserModel, {user: this.user, subunit:true})
                : null,
                
            m("a", {href: "/strike/" + this.strike.id, oncreate: m.route.link}, [
                m("div.strike", [
                    m("div.tier." + this.strike.tier),

                    m("div.text", [
                        m("div.points", (this.points).toString()+"p left"),
                        m(Clock, {date: this.expires}),
                    ]),

                    m(Progressbar, {val:this.points, max: this.strike.points}),
                ])
            ])
        ])
    }
}

module.exports = StrikeModel