var m = require("mithril")

var Progressbar = {
    oninit: function(vnode) {
        this.percentage = ((vnode.attrs.val/vnode.attrs.max)*100).toString() + "%"
    },
    view: function(vnode) {
        return m(".meter", [
            m("span", {style: {width: this.percentage}})
        ])
    }
}

module.exports = Progressbar