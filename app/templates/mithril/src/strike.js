var m = require("mithril")

var Clock = require("./components/objects/Clock")

var Loader = {
    strike: {},
    loading: m(".loading", "Loading..."),
    loadStrike: function(id) {
        return m.request({
            method: "GET",
            url: "/strike/" + id,
            withCredentials: true,
        })
        .then(Loader.loadedStrike)
        .catch(function(e) {
            Loader.error = e.message
            Loader.strikes = {}
        })
    },
    loadedStrike: function(result){
        Loader.loading = null;
        if (result === null) {
            Loader.error = "strike not found"
            Loader.strikes = {}
            return
        }
        Loader.strike = result
        if(Loader.strike.attachment) Loader.strike.attachment = Loader.convertLinks(Loader.strike.attachment)
    },    
    convertLinks: function(text) {
        var link = text.match(/(?:www|https?)[^\s]+/g),
            aLink = [],
            newText = [],
            replacedText = text;
    
        if (link != null) {
            for (i=0;i<link.length;i++) {
                var replace;

                if (!( link[i].match(/(http(s?))\:\/\//) ) ) { 
                    replace = 'http://' + link[i]; 
                } else { 
                    replace = link[i] 
                };

                var linkText = replace.split('/')[2];
                newText.push(m("span", replacedText.split(link[i])[0]))
                newText.push(m("a.parsed-link", {href: replace}, linkText));
                replacedText = replacedText.split(link[i])[1]
            }
            newText.push(replacedText)
            return newText
    
        } else {
            return text
        }
    },
}

module.exports = {
    oninit: function(vnode) {
        Loader.error = "",
        Loader.loadStrike(vnode.attrs.key)
    },
    view: function() {
        if(Loader.error) return m("h2.error", Loader.error)
        return m(".single-strike", [
            m(".tier." + Loader.strike.tier),
            m("h1", Loader.strike.points-Loader.strike.times_expired + "/" + Loader.strike.points + "p left"),

            m(".strike-info-wrap", [
                m("section.fullwidth", [
                    m(".section-header", "Reason"),
                    m(".section-value", Loader.strike.reason),
                ]),
                m("section.fullwidth", [
                    m(".section-header", "Attachment"),
                    m(".section-value", Loader.strike.attachment),
                ]),
                Loader.strike.next_expire ? m("section", [
                    m(".section-header", "Next expire"),
                    Loader.strike.next_expire ? m(Clock, {date: Loader.strike.next_expire}) : Loader.strike.next_expire
                ]) : null,
                m("section", [
                    m(".section-header", "Created on"),
                    m(".section-value", Loader.strike.created_on)
                ]),
                m("section", [
                    m(".section-header", "Created by"),
                    m("a", {href: "/user/"+Loader.strike.created_by, oncreate: m.route.link}, [
                        m(".section-value", Loader.strike.created_by)
                    ])
                ]),
                Loader.strike.removed_by ? m("section", [
                    m(".section-header", "Removed by"),
                    m("a", {href: "/user/"+Loader.strike.removed_by, oncreate: m.route.link}, [
                        m(".section-value", Loader.strike.removed_by)
                    ])
                ]) : null
            ])
        ], Loader.loading)
    }
}