import m from "mithril";

var Layout = require("./views/Layout")

var Overview = require("./Overview")
var Active = require("./Active")
var Users = require("./Users")
var All = require("./All")
var Bans = require("./Bans")
var Strike = require("./Strike")
var User = require("./User")

var page = isMod ? "/overview" : "/user/me"

m.route(document.querySelector("#interface-wrap"), page, {
    "/overview": {
        render: function() {
            return m(Layout, m(Overview))
        }
    },
    "/active": {
        render: function() {
            return m(Layout, m(Active))
        }
    },
    "/users": {
        render: function() {
            return m(Layout, m(Users))
        }
    },
    "/all": {
        render: function() {
            return m(Layout, m(All))
        }
    },
    "/bans": {
        render: function() {
            return m(Layout, m(Bans))
        }
    },
    "/strike/:key": {
        render: function(vnode) {
            return m(Layout, m(Strike, vnode.attrs))
        }
    },
    "/user/:key": {
        render: function(vnode) {
            return m(Layout, m(User, vnode.attrs))
        }
    },
})