var m = require("mithril")

var StrikeModel = require("./components/objects/StrikeModel")

var Loader = {
    loading: m(".loading", "Loading..."),
    user: {},
    strikes: [],
    loadUser: function(id) {
        return m.request({
            method: "GET",
            url: "/user/" + id,
            withCredentials: true,
        })
        .then(Loader.loadedUser)
        .catch(function(e) {
            Loader.error = e.message
            Loader.user = {}
            Loader.strikes = []
        })
    },
    loadedUser: function(result){
        Loader.loading = null
        if (result === null) {
            Loader.error = "user not found"
            Loader.user = {}
            Loader.strikes = []
            return
        }
        Loader.user = result.user
        Loader.strikes = result.strikes ? result.strikes : null
    }
}

module.exports = {
    oninit: function(vnode) {
        Loader.error = ""
        Loader.loadUser(vnode.attrs.key);
    },
    view: function() {
        if(Loader.error) return m("h2.error", Loader.error)
        return m(".single-user", [
            m(".avatar", {style: {background: "url(" + Loader.user.avatar + ") #fff", backgroundSize: "7rem 7rem"}}),
            m("h1", Loader.user.name),
            m("h2", Loader.user.strike_points + "/12 points"),
            
            Loader.strikes ? m(".strike-list", [
                Loader.strikes.map(function(strike) {
                    return m(StrikeModel, {strike: strike, key: Loader.user.id})
                })
            ]) : null
        ], Loader.loading)
    }
}