var m = require("mithril")

var Loader = {
    list: {},
    loading: m(".loading", "Loading..."),
    loadList: function() {
        return m.request({
            method: "GET",
            url: "overview",
            withCredentials: true,
        })
        .then(Loader.loadedList)
    },
    loadedList: function(result){
        Loader.loading = null
        Loader.list = result
    }
}

module.exports = {
    oninit: Loader.loadList,
    view: function() {
        return m(".overview-list", [
            Object.keys(Loader.list).map(function(key) {
                return m(".overview-wrap", [
                    m(".title", [key]),
                    m(".value", Loader.list[key])
                ])
            })
        ], Loader.loading)
    }
}