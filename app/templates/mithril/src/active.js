var m = require("mithril")

var StrikeModel = require("./components/objects/StrikeModel")

var Loader = {
    list: [],
    loading: m(".loading", "Loading..."),
    loadList: function() {
        return m.request({
            method: "GET",
            url: "active",
            withCredentials: true,
        })
        .then(Loader.loadedList)
    },
    loadedList: function(result){
        Loader.loading = null
        Loader.list = result.sort(Loader.compare)
    }
}

module.exports = {
    sortName: function(a,b) {
        var A = a.user.name
        var B = b.user.name
        if (A > B) return 1;
        else if (B > A) return -1;
        return 0
    },
    oninit: Loader.loadList,
    view: function() {
        return m(".strike-list-wrapper", [
            m(".strike-list", [
                Loader.list.sort(this.sortName).map(function(data) {
                    return m(StrikeModel, {strike: data.strike, user: data.user})
                })
            ], Loader.loading)
        ])
    }
}