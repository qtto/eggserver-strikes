var m = require("mithril")

var UserModel = require("./components/objects/UserModel")

var Loader = {
    list: [],
    loading: m(".loading", "Loading..."),
    loadList: function() {
        return m.request({
            method: "GET",
            url: "bans",
            withCredentials: true,
        })
        .then(Loader.loadedList)
    },
    loadedList: function(result){
        Loader.loading = null;
        Loader.list = result;
    }
}

module.exports = {
    oninit: Loader.loadList,
    view: function() {
        return m(".user-list", [Loader.list.map(function(data) {
            return m(UserModel, {user: data})
        })], Loader.loading)
    }
}