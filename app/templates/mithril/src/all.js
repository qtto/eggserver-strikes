var m = require("mithril")

var StrikeModel = require("./components/objects/StrikeModel")

var Loader = {
    list: [],
    loading: m(".loading", "Loading..."),
    loadList: function() {
        return m.request({
            method: "GET",
            url: "all",
            withCredentials: true,
        })
        .then(Loader.loadedList)
    },
    loadedList: function(result){
        Loader.loading = null;
        Loader.list = result;
    }
}

module.exports = {
    oninit: Loader.loadList,
    view: function() {
        return m(".strike-list", [Loader.list.map(function(data) {
            return m(StrikeModel, {strike: data.strike, user: data.user})
        })], Loader.loading)
    }
}