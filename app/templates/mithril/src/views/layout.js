var m = require("mithril")

var Nav = require("../components/Nav")

module.exports = {
    view: function(vnode) {
        return m("main.layout", [
            m("nav.menu", m(Nav)),
            m("section", vnode.children)
        ])
    }
}