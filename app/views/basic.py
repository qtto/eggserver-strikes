# Import flask dependencies
from flask import Blueprint, render_template, flash,  redirect, url_for

# Import the database object from the main app module
from app import flask_app
from flask_login import current_user

from app.utils.checks import mod_required, login_required

# Generic routes
@flask_app.route("/")
def index():
    return render_template("index.html")


# HTTP error handling
@flask_app.errorhandler(404)
@flask_app.errorhandler(401)
@flask_app.errorhandler(500)
def unauthorized(error):
    return render_template('error.html', error=error, code=error.code), error.code