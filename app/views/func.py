from flask import Blueprint, request, render_template, flash, redirect, url_for, jsonify

from app import flask_app, db
from app.utils.checks import login_required, mod_required
from app.utils.func import get_bans, sanity_check, get_expired, migrate_db

# routes
@flask_app.route('/update')
@mod_required
def update():
    if get_bans():
        try:
            sanity_check()
            get_expired()
        except:
            pass
        return ('', 200)
    return ('', 500)


@flask_app.route('/update_expired')
@mod_required
def update_expired():
    return jsonify({"expired": get_expired()})


@flask_app.route('/migrate')
@mod_required
def migrate():
    return jsonify({"migrated": migrate_db()})