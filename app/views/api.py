from flask import jsonify, url_for
from flask_restful import Api, Resource, reqparse
from app.utils.checks import login_required, mod_required, current_user
import app.utils.func as func
from app.utils import queries
from app import flask_app, db
from app.models import Ban, User, Strike
from werkzeug.exceptions import Unauthorized

flask_api = Api(flask_app)

# API routes
# Overview
class Overview(Resource):
    @mod_required
    def get(self):
        json = {
            "Total strikes": queries.count_total_strike(),
            "Active strikes": queries.count_active_strike(),
            "Site users": queries.count_site_user(),
            "Banned users": queries.count_ban(),
            "Strikes given by you": queries.count_personal_strike_given(),
            "Total strike points": queries.count_total_strike_points()
        }

        return jsonify(json)

flask_api.add_resource(Overview, '/overview')


# Strikes for specific user
class UserInfo(Resource):
    @login_required
    def get(self, user_id):
        if user_id == "me":
            user_id = current_user.get_id()   
        else:
            try: 
                int(user_id)
            except ValueError:
                uid = queries.get_user_id_from_name(user_id)
                if not uid:
                    return ('', 204)
                user_id = uid

        # in case the user is a bot
        if not current_user.is_authenticated:
            is_mod = True
        else:
            is_mod = current_user.get_is_mod()

        if current_user.get_id() != user_id:
            if not is_mod:
                raise Unauthorized
  
        func.update_user(user_id)
        strikes = queries.get_personal_strike(user_id, no_removed=not is_mod)

        if strikes:
            json = func.format_strikes(strikes, for_mod=is_mod, single_user=True)
            return jsonify(json)
        
        # if the user doesnt have strikes, still attempt to return the user info
        elif user_id:
            try:
                user = func.id_to_user(user_id)
                json = {"user": func.format_user_info(user)}
                return jsonify(json)
            except:
                pass

        return ('', 204)

flask_api.add_resource(UserInfo, '/user/<string:user_id>')


# Single strike
class StrikeInfo(Resource):
    @login_required
    def get(self, strike_id): 
        strike = queries.get_single_strike(strike_id)

        if not strike or current_user.get_id() != strike.user.get_id():
            if not current_user.get_is_mod():
                raise Unauthorized
            
            if not strike:
                return ('', 204)

        json = func.format_single_strike(strike, for_mod=current_user.get_is_mod())
        
        return jsonify(json)

    
    @mod_required
    def delete(self, strike_id):
        strike = queries.get_single_strike(strike_id)
        
        if not strike:
            return ('', 204)

        if current_user.is_authenticated:
            removed_by = current_user.get_id()
        else:
            removed_by = None

        try:
            # If not removed_by the user is the bot
            if not removed_by:
                parser = reqparse.RequestParser()
                parser.add_argument('removed_by', required=True, type=str, help='deleted by whom')
                args = parser.parse_args()
                removed_by = args["removed_by"]

            strike.removed = True
            strike.removed_by = removed_by
            strike.next_expire = None
            db.session.add(strike)
            db.session.commit()

            return ('', 200)
        except Exception as e:
            return ('Error removing strike {}'.format(e), 500)

flask_api.add_resource(StrikeInfo, '/strike/<string:strike_id>')


# Create strike
class CreateStrike(Resource):
    @mod_required
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument('user_id', required=True, type=str, help='User to strike')
        parser.add_argument('tier_id', required=True, type=int, help='Level of tier for the strike')
        parser.add_argument('reason', required=True, type=str, help='Reason for strike')
        parser.add_argument('attachment', type=str, help='Optional: attachments')
        parser.add_argument('created_by', required=True, type=str, help='Created by whom')
        args = parser.parse_args()

        strike = Strike(
            user_id = args["user_id"],
            tier_id = args["tier_id"],
            reason = args["reason"],
            attachment = args["attachment"],
            created_by = args["created_by"]
        )

        if func.add_strike(strike):
            user = queries.get_user_from_id(args["user_id"])
            mute = queries.get_mute_duration(args["user_id"])

            json = {
                "user": user.nick,
                "mute": str(mute),
                "user_points": user.points
            }
            return jsonify(json)
        
        return ('', 500)


flask_api.add_resource(CreateStrike, '/strike')


# Active strikes
class Active(Resource):
    @mod_required
    def get(self):
        strikes = queries.get_active_strike()

        if not strikes:
            return ('', 204)

        json = func.format_strikes(strikes, for_mod=True)

        return jsonify(json)

flask_api.add_resource(Active, '/active')


# All strikes
class All(Resource):
    @mod_required
    def get(self):
        strikes = queries.get_total_strike()

        if not strikes:
            return ('', 204)

        json = func.format_strikes(strikes, for_mod=True)

        return jsonify(json)

flask_api.add_resource(All, '/all')


# All users sorted by strike
class AllUsers(Resource):
    @mod_required
    def get(self):
        users = queries.get_total_user()

        if not users:
            return ('', 204)

        json = []
        for user in users:
            if user.points > 0:
                json.append({"id": user.user_id, "points":user.points, "name":user.nick, "avatar":func.make_avatar(user)})
        
        return jsonify(json)

flask_api.add_resource(AllUsers, '/users')


# Ban list
class BanList(Resource):
    @mod_required
    def get(self):
        bans = Ban.query.all()

        json = []
        for ban in bans:
            try:
                json.append({
                    "name": ban.user.nick,
                    "reason": ban.reason,
                    "avatar": func.make_avatar(ban.user),
                    "points": ban.user.points
                })
            except:
                json.append({
                    "name": "Unknown user",
                    "reason": "Unknown reason",
                    "avatar": "",
                    "points": "?"
                })

        return jsonify(json)

flask_api.add_resource(BanList, '/bans')


# Username from id
class UserName(Resource):
    @mod_required
    def get(self, user_id): 
        username = func.id_to_user(user_id).nick
        
        if username:
            return jsonify(username)
        
        return jsonify(user_id)
        
flask_api.add_resource(UserName, '/username/<string:user_id>')