from flask import Blueprint, request, render_template, flash, redirect, url_for

from flask_dance.contrib.discord import make_discord_blueprint, discord
from flask_dance.consumer import oauth_authorized
from flask_dance.consumer.backend.sqla import SQLAlchemyBackend
from flask_login import current_user, LoginManager, login_user, logout_user

from app import flask_app, db
from app.models import OAuth, User
from app.utils.checks import login_required
from app.utils.func import get_mod
from app.utils.queries import get_user_from_id

login_manager = LoginManager(flask_app)

@login_manager.user_loader
def load_user(id):
    return User.query.get(str(id))

discord_bp = make_discord_blueprint(
    scope = ["identify"],
    backend = SQLAlchemyBackend(OAuth, db.session, user=current_user),
    login_url = "/",
    authorized_url = "/callback"
)
flask_app.register_blueprint(discord_bp, url_prefix="/login")


@oauth_authorized.connect_via(discord_bp)
def discord_logged_in(blueprint, token):
    if not token:
        flash("Failed to log in with Discord.", category="error")
        return False

    resp = blueprint.session.get("/api/users/@me")
    if not resp.ok:
        msg = "Failed to fetch user info from Discord."
        flash(msg, category="error")
        return False

    discord_json = resp.json()
    discord_user_id = str(discord_json["id"])
    discord_name = str(discord_json["username"])
    discord_avatar = str(discord_json["avatar"])
    user_is_mod = get_mod(discord_user_id)

    # Find this OAuth token in the database, or create it
    query = OAuth.query.filter_by(
        provider=blueprint.name,
        provider_user_id=discord_user_id,
    )
    if query.scalar() is None:
        oauth = OAuth(
            provider=blueprint.name,
            provider_user_id=discord_user_id,
            token=token,
        )
    else:
        oauth = query.one()        

    if oauth.user:
        # If this OAuth token already has an associated local account,
        # log in that local user account.
        # Note that if we just created this OAuth token, then it can't
        # have an associated local account yet.
        oauth.user.avatar = discord_avatar
        oauth.user.nick = discord_name
        oauth.user.is_mod = user_is_mod
        db.session.add(oauth.user)
        db.session.commit()
        login_user(oauth.user, remember=True)
        flash("Signed in with Discord.")

    else:
        # If this OAuth token doesn't have an associated local account,
        # create a new local user account for this user. We can log
        # in that account as well, while we're at it.
        user = get_user_from_id(discord_user_id)
        if not user:
            user = User(
                user_id=discord_user_id,
                nick=discord_name,
                avatar=discord_avatar,
                is_mod=user_is_mod
            )
        else:
            user.is_mod = user_is_mod
            

        # Associate the new local user account with the OAuth token
        oauth.user = user
        # Save and commit our database models
        db.session.add_all([user, oauth])
        db.session.commit()
        # Log in the new local user account
        login_user(user, remember=True)
        flash("Signed in with Discord.")

    # Since we're manually creating the OAuth model in the database,
    # we should return False so that Flask-Dance knows that
    # it doesn't have to do it. If we don't return False, the OAuth token
    # could be saved twice, or Flask-Dance could throw an error when
    # trying to incorrectly save it for us.
    return False



# routes
@flask_app.route('/login')
def login():
    if not current_user.is_anonymous:
        return redirect(url_for('index'))
    return redirect(url_for("discord.login"))


@flask_app.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out")
    return redirect(url_for('index'))