# Import flask and template operators
from flask import Flask

# Define the WSGI application object
flask_app = Flask(__name__, instance_relative_config=True)

# Configurations
flask_app.config.from_object('config')
flask_app.config.from_pyfile('config.py')


# DB stuff
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy(flask_app)


# Initialize views
from app import views

# DB initialization
import app.models
if app.models.Tier.query.count() == 0:
    green = app.models.Tier(
        tier_id=1,
        points=1,
        name="green"
    )

    yellow = app.models.Tier(
        tier_id=2,
        points=2,
        name="yellow"
    )

    orange = app.models.Tier(
        tier_id=3,
        points=4,
        name="orange"
    )

    red = app.models.Tier(
        tier_id=4,
        points=8,
        name="red"
    )

    carmine = app.models.Tier(
        tier_id=5,
        points=12,
        name="carmine",
        can_expire=False
    )
    db.session.add_all([green, yellow, orange, red, carmine])

from datetime import timedelta
if app.models.Mute.query.count() == 0:
    mute = app.models.Mute(
        points= 2,
        duration= timedelta(hours=6)
    )
    db.session.add(mute)

    mute = app.models.Mute(
        points= 3,
        duration= timedelta(hours=12)
    )
    db.session.add(mute)

    mute = app.models.Mute(
        points= 4,
        duration= timedelta(hours=24)
    )
    db.session.add(mute)

    mute = app.models.Mute(
        points= 5,
        duration= timedelta(hours=48)
    )
    db.session.add(mute)

    mute = app.models.Mute(
        points= 6,
        duration= timedelta(hours=72)
    )
    db.session.add(mute)

    mute = app.models.Mute(
        points= 7,
        duration= timedelta(hours=96)
    )
    db.session.add(mute)

    mute = app.models.Mute(
        points= 8,
        duration= timedelta(hours=120)
    )
    db.session.add(mute)

    mute = app.models.Mute(
        points= 9,
        duration= timedelta(hours=144)
    )
    db.session.add(mute)

    mute = app.models.Mute(
        points= 10,
        duration= timedelta(hours=168)
    )
    db.session.add(mute)

    mute = app.models.Mute(
        points= 11,
        duration= timedelta(hours=216)
    )
    db.session.add(mute)

    db.session.commit()


if app.models.Strike.query.count() == 0:
    from datetime import datetime
    with open("strikes.csv", "r") as f:
        for line in f:
            line = line.split(';')
            strike = app.models.Strike(
                user_id = line[1],
                tier_id = line[2],
                reason = line[3],
                attachment = line[4],
                created_on  = datetime.strptime(line[5], "%d-%m-%Y %H:%M:%S"),
                created_by = line[6]
            )
            db.session.add(strike)
    db.session.commit()