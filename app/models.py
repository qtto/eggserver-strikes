from app import db
from functools import reduce
from sqlalchemy.ext.hybrid import hybrid_property
from flask_dance.consumer.backend.sqla import OAuthConsumerMixin
from flask_login import UserMixin

# Discord Users
class User(UserMixin, db.Model):
    __tablename__ = "users"
    user_id = db.Column(db.String, primary_key=True)
    nick = db.Column(db.String,  nullable=False)
    avatar = db.Column(db.String)
    is_mod = db.Column(db.Boolean, default=False, nullable=False)
    strikes = db.relationship("Strike", backref="users", lazy=True)

    def __repr__(self):
        return "<User {}>".format(self.user_id)

    def get_id(self):
        return self.user_id

    def get_name(self):
        return self.nick

    def get_avatar(self):
        return self.avatar

    def get_is_mod(self):
        return self.is_mod

    @hybrid_property
    def points(self):
        strikes = Strike.query.filter_by(user_id=self.user_id)\
                          .filter_by(expired=False)\
                          .filter_by(removed=False).all()
        
        points = reduce(lambda x, y: x + y.tier.points, strikes, 0)
        exp = reduce(lambda x, y: x + y.times_expired, strikes, 0)

        return points - exp


# OAuth Backend
class OAuth(OAuthConsumerMixin, db.Model):
    provider_user_id = db.Column(db.String)
    user_id = db.Column(db.String, db.ForeignKey("users.user_id"))
    user = db.relationship("User")
    

# Strikes
class Strike(db.Model):
    __tablename__ = "strikes"
    id = db.Column(db.Integer, primary_key=True)
    
    user_id = db.Column(db.String, db.ForeignKey("users.user_id"), nullable=False)
    user = db.relationship("User")

    tier_id = db.Column(db.Integer, db.ForeignKey("tiers.tier_id"), nullable=False)
    tier = db.relationship("Tier")
    
    reason = db.Column(db.String(512),  nullable=False)
    attachment = db.Column(db.String(512))

    created_on  = db.Column(db.DateTime,  default=db.func.current_timestamp(), nullable=False)
    created_by = db.Column(db.String,  nullable=False)

    times_expired = db.Column(db.Integer, default=0, nullable=False)
    last_expired  = db.Column(db.DateTime)
    next_expire  = db.Column(db.DateTime)
    expired = db.Column(db.Boolean, default=False, nullable=False)

    removed = db.Column(db.Boolean, default=False, nullable=False)
    removed_by = db.Column(db.String)

    def __repr__(self):
        return "<Tier {} strike for {}>".format(self.tier_id, self.reason)


# Tiers
class Tier(db.Model):
    __tablename__ = "tiers"
    tier_id = db.Column(db.Integer, primary_key=True, nullable=False)
    name = db.Column(db.String, nullable=False)
    points = db.Column(db.Integer, nullable=False)
    can_expire = db.Column(db.Boolean, default=True, nullable=False)


# Mutes
class Mute(db.Model):
    __tablename__ = "mutes"
    points = db.Column(db.Integer, primary_key=True, nullable=False)
    duration  = db.Column(db.Interval, nullable=False)


# Tiers
class Ban(db.Model):
    __tablename__ = "bans"
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.String, db.ForeignKey("users.user_id"))
    user = db.relationship("User")
    reason = db.Column(db.String)

# Run the model
db.create_all()
